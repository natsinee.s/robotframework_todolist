*** Variables ***
${URL}  https://abhigyank.github.io/To-Do-List/
${BROWSER}  chrome
${MESSAGE}  I'm working
${toDoListText}    //*[contains(text(),'To Do List')]
${addButton}    //*[@data-upgraded=",MaterialButton"]
${inputText}    //input[@id='new-task']
${taskName}     test-v1
${toDoTaskTab}          //*[@href="#todo"]
${validateGroupText}    //*[contains(text(),'test-v1')]
${deleteButton}         //*[@class="mdl-button mdl-js-button mdl-js-ripple-effect delete"]
${completedTab}         //*[@href="#completed"]
${targetCheckBox}       //body/div[1]/div[1]/div[3]/ul[1]/li[1]/label[1]/span[4]
*** Keywords ***
Open toDoList Browser
    Open Browser  ${URL}  ${BROWSER}
    Log to console  ${MESSAGE}

landing on toDoList page
    Wait Until Element Is Visible     ${toDoListText} 
    Capture Page Screenshot
validate add button
    Wait Until Element Is Visible   ${addButton}
    Capture Page Screenshot
add new toDoList
    Input Text      ${inputText}        ${taskName}
    Click Element   ${addButton}
    Capture Page Screenshot
validate addedList
    Click Element   ${toDoTaskTab} 
    Wait Until Element Is Visible   ${validateGroupText} 
    Capture Page Screenshot
validate delete button
    Wait Until Element Is Visible       ${deleteButton}
    Click Element   ${deleteButton}
validate task has been deleted
    Wait Until Element Is Not Visible       ${deleteButton}
    Capture Page Screenshot
validate completedTap
    Click Element   ${completedTab} 
    Capture Page Screenshot
completed checkbox
    Wait Until Element Is Visible       ${targetCheckBox} 
    Click Element   ${targetCheckBox}    
    Capture Page Screenshot

Precondition case2
    Open toDoList Browser
    landing on toDoList page
    validate add button
    add new toDoList
    validate addedList
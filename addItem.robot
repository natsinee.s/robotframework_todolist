*** Settings ***
Library         Selenium2Library
Test Teardown   Close All Browsers
Suite Setup     Log to console  Hello World
Suite Teardown  Log to console  Goodbye!!!
Resource        commonKeyword.robot
Resource        commonVariable.robot

*** Test Cases ***
add new to-do list
    Open toDoList Browser
    landing on toDoList page
    validate add button
    add new toDoList
    validate addedList